package com.erp.anhdn.dto;
import lombok.Data;

@Data // không phải thêm getter setter
public class UserDTO {
    private int id;
    private String username;
    private String password;
    private String fullName;
    private int idRole;
    private String phone;
    private String email;
    private int status;

}
